# 1. Apa itu Class Diagram?
Class Diagram adalah jenis diagram struktur statis dalam UML yang menggambarkan struktur sistem dengan menunjukkan sistem class, atributnya, metode, dan hubungan antar objek.

# 2. Mengapa Class Diagram digunakan?
Fungsi utama dari class diagram adalah menggambarkan struktur sebuah sistem pemrograman. Meski demikian, terdapat beberapa fungsi lainnya dari class diagram.
- Menunjukkan struktur statis pengklasifikasi dalam suatu sistem.
- Memberikan notasi dasar untuk diagram struktur lain yang ditentukan oleh UML.
- Dapat digunakan business analyst untuk membuat model sistem dari perspektif bisnis.
# 3. Komponen Class Diagram
![dokumentasi](dokumentasi/komponen.jpg)
1. Komponen atas
Bagian ini berisikan nama class yang selalu diperlukan baik itu dalam pengklasifikasi atau objek.

2. Komponen tengah
Komponen ini berisikan atribut class yang digunakan untuk mendeskripsikan kualitas kelas. Ini hanya diperlukan saat mendeskripsikan instance tertentu dari class.

3. Komponen bawah
Bagian ini adalah komponen class diagram yang menyertakan operasi class yang ditampilkan dalam format daftar. Sehingga, setiap operasi mengambil barisnya sendiri.
# 4. Hubungan Antar Class Diagram
Ada enam jenis utama hubungan antar kelas: pewarisan, realisasi/implementasi, komposisi, agregasi, asosiasi, dan ketergantungan. Tanda panah untuk enam hubungan adalah sebagai berikut:

![dokumentasi](dokumentasi/relasi.jpeg)

Pada keenam tipe relasi, struktur kode dari ketiga tipe relasi seperti komposisi, agregasi, dan asosiasi sama dengan menggunakan atribut untuk menyimpan referensi dari kelas lain. Oleh karena itu, keduanya harus dibedakan berdasarkan hubungan antar isinya.

1.  Warisan
yaitu hubungan hirarkis antar class. Class dapat diturunkan dari class lain dan mewarisi semua atribut dan metoda class asalnya dan menambahkan fungsionalitas baru, sehingga ia disebut anak dari class yang diwarisinya. Kebalikan dari pewarisan adalah generalisasi. Contoh: Hubungan antara kelas "Mobil" dan "Sedan". Kelas "Sedan" mewarisi sifat dan perilaku dari kelas "Mobil". Ini berarti "Sedan" memiliki semua atribut dan metode yang dimiliki oleh "Mobil", dan juga dapat memiliki atribut dan metode tambahan yang spesifik untuk "Sedan".
2. Realisasi / Implementasi
menunjukkan implementasi sebuah kelas terhadap sebuah interface. Contoh: Hubungan antara kelas "Karyawan" dan "Gaji". Kelas "Karyawan" mengimplementasikan interface "Gaji", yang berarti "Karyawan" harus menyediakan implementasi untuk metode-metode yang didefinisikan dalam interface "Gaji".
3. Relasi komposisi 
menunjukkan bahwa sebuah kelas terdiri dari bagian-bagian yang merupakan bagian integral dari kelas tersebut. Contoh: Hubungan antara kelas "Rumah" dan "Kamar". Kelas "Rumah" terdiri dari beberapa objek "Kamar". Jika "Rumah" dihapus, maka semua objek "Kamar" di dalamnya juga akan terhapus.
4. Hubungan Agregasi
dalah hubungan agregasi ini di mana satu kelas memiliki objek dari kelas lain sebagai bagian darinya. Contoh: Hubungan antara kelas "Sekolah" dan "Guru". Kelas "Sekolah" memiliki objek "Guru" sebagai bagian darinya. Namun, jika "Sekolah" dihapus, objek "Guru" masih dapat ada dan terkait dengan entitas lain.
5. Hubungan Asosiasi
yaitu hubungan statis antar class. Umumnya menggambarkan class yang memiliki atribut berupa class lain, atau class yang harus mengetahui eksistensi class lain. Panah navigability menunjukkan arah query antar class. Contoh: Hubungan antara kelas "Pemesan" dan "Hotel". Kelas "Pemesan" memiliki asosiasi dengan kelas "Hotel" yang menunjukkan bahwa seorang pemesan dapat memesan kamar di sebuah hotel.
6. Ketergantungan
terjadi ketika sebuah kelas menggunakan atau bergantung pada kelas lain tanpa adanya hubungan langsung. Contoh: Hubungan antara kelas "Penulis" dan "Buku". Kelas "Buku" bergantung pada kelas "Penulis" karena setiap buku harus memiliki penulis yang mengarangnya. Jika kelas "Penulis" mengalami perubahan, hal ini juga dapat memengaruhi kelas "Buku".
# 5. Modifier
Tanda | Deskripsi
--- | ---
\+ | Modifier public
\- | Modifier private
\# | Modifier protected

# 6. Notasi Hubungan Antar Class Diagram
```mermaid
classDiagram
classA --|> classB : Inheritance
classC --* classD : Composition
classE --o classF : Aggregation
classG --> classH : Association
classI -- classJ : Link(Solid)
classK ..> classL : Dependency
classM ..|> classN : Realization
classO .. classP : Link(Dashed)
```
# 7. Contoh Class Diagram

- Class Diagram menggunakan https://app.diagrams.net/ 

![dokumentasi](dokumentasi/classdiagram.jpeg)

- Class Diagram menggunakan mermaid.js 

```mermaid
classDiagram
    class anggota_perpustakaan {
    +id_anggota : int
    +nama_anggota : char
    +insert_data()
    +update_data()
    }

    class peminjaman_buku {
    +id_anggota : int
    +id_buku : int
    +id_petugas : int
    +idtanggal_peminjaman : date
    +idtanggal_pengembalian : date
    +insert_data()
    +update_data()
    +pilih_buku()
    }

    class buku {
    +id_buku : int
    +judul_buku : char
    +penulis_buku : char
    +genre_buku : char
    +tahun_terbit_buku : char
    +insert_data()
    +update_data()
    }

    class buku_asing {
    +id_buku : int
    +judul_buku : char
    +penulis_buku : char
    +genre_buku : char
    +tahun_terbit_buku : char
    +insert_data()
    +update_data()
    }

    class buku_lokal {
    +id_buku : int
    +judul_buku : char
    +penulis_buku : char
    +genre_buku : char
    +tahun_terbit_buku : char
    +insert_data()
    +update_data()
    }

    class petugas_perpustakaan {
    +id_petugas : int
    +nama_petugas : char
    +insert_data()
    +update_data()
    }

    buku <|-- buku_asing
    buku <|-- buku_lokal
    anggota_perpustakaan -- peminjaman_buku
    peminjaman_buku -- buku
    peminjaman_buku -- petugas_perpustakaan
```
# 8. Penjelasan Class Diagram
https://youtu.be/EGf4zVqeTPE
# 9. Referensi
- https://glints.com/id/lowongan/class-diagram-adalah/#:~:text=nya%20di%20Sini-,Definisi%20Class%20Diagram,metode%2C%20dan%20hubungan%20antar%20objek.

- https://www.smktarunabangsa.sch.id/artikel/detail/class-diagram
